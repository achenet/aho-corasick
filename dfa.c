#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

#define CAPACITY 5000

/* basic deterministic finite state automaton that 
   checks in line return terminated input string
   matches regex : aa */


   // a dfa is:
   // a set of states
   // a transition function that takes as input a letter of a finite set of symbols and a state
   // and outputs another state.
   // one state is marked Initial
   // at least one is marked Final.

const int States[] = {0, 1, 2};

const int InitialState = 0;

const int FinalState = 2;

// Using a hand-coded function to handle transitions right now
// next steps, figure out how hashmaps work next

int transition(int state, char letter) {
   if (state == 0 && letter == 'a')
      return 1;
   if (state == 1 && letter == 'a')
      return 2;
   if (state == 2 && letter == 'a') {
      return 2;
   }
   return 0;
}

int main()
{
   // read user input
   char buf[CAPACITY];
   printf("input your text:\n");
   scanf("%s", buf);
   int len = strlen(buf);
   printf("you wrote:\n%s\nlength: %d", buf, len);
   // go through the text and see where it ends up, starting
   // on the initial state
   int state = 0;
   for (int i = 0; i < len; ++i)
   {
      char letter = buf[i];
      printf("\ncurrent state: %d, letter: %c", state, letter);
      state = transition(state, letter);
   }
   printf("\n");
   if (state == 2) {
      printf("final state: %d is accepting, your language is recognized by the automata", state); 
   } else {
         printf("final state: %d is NOT accepting, your language is NOT recognized by the automata", state); 
   }
   return 0;
}

