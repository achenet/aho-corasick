#include <stddef.h>
#include <stdio.h>

// https://www.digitalocean.com/community/tutorials/hash-table-in-c-plus-plus

// https://benhoyt.com/writings/hash-table-in-c/

size_t hash(char input[],  size_t arr_len,int length) {
  int out = 0;
  // iterate over elements of a char[]
  for (int i = 0; i < arr_len; ++i)
  {
    out = out * 31 + input[i];
    printf("out: %d\n", out);
  }
  return out % length;
}


int main() {
  printf("hash table tiem :DDD\n");

  // how do hash tables work?
  // let's assume we have a working hash function.
  // how do we build something that lets us look up the value associated
  // with a key in near constant time?
  printf("hash: %zu", hash("toto", 4, 7));
  
  return 0;
}

// what hash function to use?
// we can use a bad one, namely 
// add all the elements of the array
// and divide modulo size of hash table
// or...
// we can get fancy and try a *REAL* hash function

// Ben Hoyt uses the FNV-1a hash function
// very similar to FNV-1 hash
// ... on Noll's website they recommend using the FNV-1a version
// though.

// there's also SipHash, which is better against
// Hash flooding DoS attacks... because that should
// totally be a factor to take under consideration...
// eh, can always implement multiple hash functions
// and just pick you favorite <3

// ghetto hash function:
// take the int value of each char, multiply it by 31
// sum, modulo length
